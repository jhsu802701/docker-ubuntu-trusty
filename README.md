# docker-ubuntu-trusty
Welcome to my repository for using my Docker images based on Ubuntu Trusty.  These Docker images are stored on [Docker Hub](https://hub.docker.com/r/jhsu802701/).  These Docker images are customized for specific projects.

## Prerequisites
Go to https://gitlab.com/rubyonracetracks/docker-debian-stretch.  The same prerequisites, warnings, and procedures that apply there also apply here.

## Building Docker Images
The scripts for building these Docker images are at https://bitbucket.org/jhsu802701/docker-ubuntu-trusty-build.
